module.exports = {
    purge: [
        './src/**/*.html',
        './src/**/*.vue'
    ],
    theme: {
        extend: {
            colors: {
                'btn': {
                    'normal': '#000000',
                    'hover': '#FFD24D',
                    'active': '#3A3A3A',
                    'text': '#F1F1F1'
                },
            },
            maxHeight: {
                '2/3': '66vh',
            },
        },
    },
    variants: {
        borderWidth: ['responsive', 'hover', 'focus'],
        backgroundColor: ['responsive', 'hover', 'focus', 'active'],
        textColor: ['responsive', 'hover', 'focus', 'active'],
    },
    plugins: [],
}
