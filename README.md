# NIVA - Seamless claim webapp

This repo contains the backend project for the NIVA seamless claim application, the project is managed as a set of MAVEN modules;
please see README.md files in the sub directories for module specific information.

# Introduction
This sub-project is part of the ["New IACS Vision in Action” --- NIVA](https://www.niva4cap.eu/) project that delivers a
a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to
support further development of IACS that will facilitate data and information flows.
This project has received funding from the European Union’s
Horizon 2020 research and innovation programme under grant agreement No 842009.
Please visit the [website](https://www.niva4cap.eu) for
further information. A complete list of the sub-projects
made available under the NIVA project can be found on [gitlab](https://gitlab.com/nivaeu/)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
