# Docker container build

You can build the docker image, from the project root directory using the following command:

    docker build -t niva/niva-seamlessclaim-ui:1.0.0 -f docker/Dockerfile .
