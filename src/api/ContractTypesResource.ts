/*-
 * Copyright (C) 2019 - 2021 ABACO
 *
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
 */

import { http, BASE, propsToDate, listPropsToDate } from './HTTP';
import { ContractType, ContractTypeEligibilityResult} from './models/Contract';

export default class ContractTypesResource {

    public static async getCTypes(): Promise<ContractType[]>{
        const response = await http.get(`${BASE}/contract-types`)
        // listPropsToDate(response.data, ["subscribeFrom", "subscribeTo"]);
        return response.data;
    }

    public static async isEligible(partyId: string, types: string[]): Promise<ContractTypeEligibilityResult[]> {
        const response = http.get(`${BASE}/contract-types/parties/${partyId}/is-eligible`,
        {
            params:{
                type: types
            }
        })
        return (await response).data
    }

}
