/*-
 * Copyright (C) 2019 - 2021 ABACO
 *
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
 */

import { http, BASE } from "./HTTP";
import { User } from './models/User';
import i18n from "@/i18n";
import store from "@/store";
import { UserInfo } from "@/store/currentUser";

export class LoginResource {

    public static async testLoginToken(token: string) {
        http.addRequestConfigProvider(async (config) => {
            config.headers = { Authorization: `Basic ${token}` };
        });

        const res = http.get<User>(`${BASE}/test-login`);
        const user = (await res).data
        store.commit("setCurrentUser", { user: user, token: token } as UserInfo)
        await this.setLanguage(user.lang);

        return user;
    }

    public static async testLogin(username: string, password: string): Promise<User> {
        http.addRequestConfigProvider(async (config) => {
            config.auth = { username, password };
        });

        const res = http.get<User>(`${BASE}/test-login`, {
            params: {
                username,
                password
            }
        });
        const user = (await res).data;
        store.commit("setCurrentUser", { user: user, token: btoa(`${username}:${password}`) } as UserInfo)
        await this.setLanguage(user.lang);

        return user;
    }

    public static logOut(){
        store.commit("logout")
    }

    private static async setLanguage(lang: string) {
        // We are not supporting local variants
        lang = lang.replace('_', '-');
        const pos = lang.indexOf('-');
        if (pos > -1) {
            lang = lang.substring(0, pos);
        }

        try {
            const res = await http.get(`/translations/${lang}.json`);
            i18n.locale = lang;
            i18n.setLocaleMessage(lang, res.data);
            http.setLanguage(lang);
        } catch (e) {
            http.setLanguage(i18n.fallbackLocale as string);
        }
    }



}
