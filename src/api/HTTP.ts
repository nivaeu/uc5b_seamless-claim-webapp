/*-
 * Copyright (C) 2019 - 2021 ABACO
 *
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
 */

/* eslint-disable @typescript-eslint/no-explicit-any */
import axios, { AxiosPromise, AxiosRequestConfig, AxiosResponse, AxiosInstance } from "axios";

const cancelTokenSources = new Map();
const defaultConfig: AxiosRequestConfig = {
    paramsSerializer(params): string {
        const parts: string[] = [];

        Object.keys(params).forEach((key) => {
            let val = params[key];
            if (val === null || typeof val === "undefined") {
                return;
            }

            const encodedKey = encodeURIComponent(key);

            if (!Array.isArray(val)) {
                val = [val];
            }

            val.forEach((v: any) => {
                if (Object.prototype.toString.call(val) === "[object Date]") {
                    v = v.toISOString();
                } else if (typeof v === "object") {
                    v = JSON.stringify(v);
                }
                parts.push(`${encodedKey}=${encodeURIComponent(v)}`);
            });

        });

        return parts.join("&");
    }
};

interface ErrorHandler {
    /**
     * @param err the error
     * @param response the axios response, if the error was an HTTP one (i.e. 4xx, 5xx)
     */
    onResponseError(err: Error, response?: AxiosResponse): void;
}

interface HTTPClientOptions {
    errorHandler?: ErrorHandler;
    axiosConfig?: AxiosRequestConfig;
}

type RequestConfigProvider = (config: AxiosRequestConfig) => Promise<void>;

class HTTPClient {
    private axiosClient: AxiosInstance;
    private nc = new Date().getTime();
    private errorHandlers: ErrorHandler[] = [];
    private configProviders: RequestConfigProvider[] = [];

    constructor(config: HTTPClientOptions = {}) {
        const cfg = config.axiosConfig || defaultConfig;
        if (!cfg.paramsSerializer) {
            cfg.paramsSerializer = defaultConfig.paramsSerializer;
        }
        this.axiosClient = axios.create(cfg);

        if (config.errorHandler) {
            this.errorHandlers.push(config.errorHandler);
        }

        this.axiosClient.interceptors.request.use((reqConfig) => {
            const params = reqConfig.params || {};
            params._nc = this.nc++;
            reqConfig.params = params;
            if (!reqConfig.cancelToken) {
                const source = axios.CancelToken.source();

                cancelTokenSources.set(source.token, source.cancel); // join the cancel queue
                reqConfig.cancelToken = source.token;
             }
            return reqConfig;
        });

        this.axiosClient.interceptors.response.use(
            (res) => {
                if (res.config.cancelToken) {
                    cancelTokenSources.delete(res.config.cancelToken);
                }
                return res;
            },
            (err: any) => {
                if (axios.isCancel(err)) {
                    cancelTokenSources.delete(err.message)
                }

                this.notifyErrorHandlers(err, err.response);
                return Promise.reject(err);
            }
        );


    }

    private notifyErrorHandlers(err: Error, response?: AxiosResponse) {
        this.errorHandlers.forEach((h) => {
            h.onResponseError(err, response);
        });
    }

    private async provideConfig(config?: AxiosRequestConfig): Promise<AxiosRequestConfig> {
        const ret = config ? config : {};     // TODO: Deep clone config?
        for (const p of this.configProviders) {
            /* We are not firing all providers in parallel
             * to avoid the situation where 2 providers changes the same config elements
             * making debugging difficult due to possible different results at every invocation
             * we should not have more than a couple providers anyway and most of them should be synchronous
             */
            await p(ret);
        }
        return ret;
    }

    public addErrorHandler(handler: ErrorHandler) {
        this.errorHandlers.push(handler);
    }

    public addRequestConfigProvider(provider: RequestConfigProvider) {
        this.configProviders.push(provider);
    }

    public async get<T = any>(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
        return this.axiosClient.get(url, await this.provideConfig(config));
    }

    public async delete<T = any>(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
        return this.axiosClient.delete(url, await this.provideConfig(config));
    }

    public async head<T = any>(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
        return this.axiosClient.head(url, await this.provideConfig(config));
    }

    public async options<T = any>(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
        return this.axiosClient.options(url, await this.provideConfig(config));
    }

    public async post<T = any>(url: string, data?: unknown, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
        return this.axiosClient.post(url, data, await this.provideConfig(config));
    }

    public async put<T = any>(url: string, data?: unknown, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
        return this.axiosClient.put(url, data, await this.provideConfig(config));
    }

    public async patch<T = any>(url: string, data?: unknown, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
        return this.axiosClient.patch(url, data, await this.provideConfig(config));
    }

    public setLanguage(lang: string) {
        this.axiosClient.defaults.headers.common['Accept-Language'] = lang;
    }
}

function propsToDate(obj: { [key: string]: any }, keys: string[]): void {
    keys.forEach((key) => {
        const path = key.split(".");
        const lastKey = path.splice(path.length - 1, 1).join("");
        const ref = path.reduce((accum, k) => accum ? accum[k] : accum, obj);
        if (ref === undefined) {
            console.warn(`Invalid path to object, ${key} points to undefined `);
        } else if (ref !== null) {
            // Ignore null objects
            const val = ref[lastKey];
            if (val !== null) {
                if (typeof val === "string" || typeof val === "number") {
                    ref[lastKey] = new Date(val);
                } else {
                    console.warn(`Cannot convert ${val} of type ${typeof val} to Date`);
                }
            }
        }
    });
}

function listPropsToDate(list: { [key: string]: any }[], keys: string[]){
    list.forEach(el => {
        propsToDate(el, keys);
    });
}

function resolvedAxiosPromise<T = any>(data: T): AxiosPromise<T> {
    return Promise.resolve({
        data,
        status: 200,
        statusText: "OK",
        headers: [],
        config: {}
    });
}

const http = new HTTPClient();
const BASE = "/api"

export {
    AxiosPromise,
    AxiosRequestConfig,
    AxiosResponse,
    AxiosInstance,
    ErrorHandler,
    http,
    propsToDate,
    listPropsToDate,
    RequestConfigProvider,
    resolvedAxiosPromise,
    BASE,
    cancelTokenSources
};
