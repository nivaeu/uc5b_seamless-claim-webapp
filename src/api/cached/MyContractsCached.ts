/*-
 * Copyright (C) 2019 - 2021 ABACO
 *
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
 */

import ContractsResource from "../ContractsResource";
import { Contract } from "../models/Contract";

const enabled = false;

interface ContractCached {
    contracts: Contract[];
    partId: string;
    date: Date;
}

export class MyContractsCached {
    contractsMap = new Map<string, ContractCached>();

    reset() {
        this.contractsMap = new Map<string, ContractCached>();
    }

    resetPartyContracts(id: string) {
        this.contractsMap.delete(id);
    }

    async getContracts(curParty: string): Promise<Contract[]> {
        if (enabled && this.contractsMap.has(curParty)) {
            const contractCached = this.contractsMap.get(curParty);
            if (contractCached && contractCached.contracts && contractCached.contracts.length > 0) {
                return Promise.resolve(contractCached.contracts);
            }
        }
        try {
            const contracts = await ContractsResource.getContracts(
                curParty
            );
            this.contractsMap.set(curParty, {
                contracts: contracts,
                partId: curParty,
                date: new Date()
            })
            return contracts
        } catch (error) {
            console.log("Error get contracts", error);
            return []
        }
    }

}
