/*-
 * Copyright (C) 2019 - 2021 ABACO
 *
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
 */

export type EligibilityStatus = "GREEN" | "YELLOW" | "RED";

export type MonitoringState = "NOT_STARTED" |"IN_PROGRESS" | "ENDED";

export type ContractState = "INTEREST_EXPRESSED" | "UNDER_MONITORING" | "PAYABLE" | "NOT_PAYABLE" | "PAID" | "CLOSED" | "WITHDRAWN";

export const statusKeys = [
    "INTEREST_EXPRESSED",
    "UNDER_MONITORING",
    "PAYABLE",
    "NOT_PAYABLE",
    "PAID",
    "CLOSED",
    "WITHDRAWN"
];

export const statusDescriptionKeys = [
    "interestExpressedLegend",
    "underMonitoringLegend",
    "payableLegend",
    "paymentAcceptedLegend",
    "withdrawnLegend",
    "notpayableLegend"
];

export type OrderByCountEnum = "id" | "contractTypeId" | "statusCode" | "subscribtionDt";
export type OrderDirEnum = "ASC" | "DESC"
