/*-
 * Copyright (C) 2019 - 2021 ABACO
 *
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
 */

import { ContractState, EligibilityStatus, MonitoringState } from "./ContractEnumStates";

export interface Contract {
    id: string;
	contractTypeId: string;
    partyId: string;
    partyDescription: string;
	subscribtionDt: Date;
	subscribedBy: string;
    statusCode: ContractState;
    expiryDate: Date;
}


export interface ContractType {
    id: string;
    shortDescr: string,
    subscribeFrom: Date;
    subscribeTo: Date;
    durationYears: number;
    termsCond: string;
    eligible?: EligibilityStatus;
}

export interface ContractTypeEligibilityResult {
    contractTypeId: string;
    eligibilityStatus: EligibilityStatus;
    messages: EligibilityMessage[];
    warnings: string[];
}

export interface ContractDetail{
    type: ContractType;
    eligibilityResults: ContractTypeEligibilityResult;
    contract?: Contract;
    contractMonitoringStatus?: ContractMonitoringStatus | null
}

export interface ContractLandUse{
    contractId: string;
    landUseId: string;
	descr: string;
	area: number;
	monitoringStatus: string;
}

export interface EligibilityMessage {
    message: string,
    eligibile: boolean,
    url: string
}

export interface ContractMonitoringStatus {
    contractId: string,
    eligibilityStatus: EligibilityStatus,
    messages: EligibilityMessage[],
    monitoringState: MonitoringState,
    qualityIndex: number,
    referenceDate: Date | null,
    anomalies: string[]
}

export interface ContractOut {
    id: string,
    contractTypeId: string,
    partyDescription: string;
    partyId: string,
    subscribtionDt: Date,
    subscribedBy: string,
    statusCode: ContractState,
    expiryDate: Date
}
