/*-
 * Copyright (C) 2019 - 2021 ABACO
 *
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
 */

import { OrderByCountEnum, OrderDirEnum } from "./ContractEnumStates";

export interface ParamPaginated {
    contractId?: string | undefined,
    contractTypeId?: string | undefined,
    status?: string | undefined,
    farm?: string | undefined,
    offset?: number | undefined,
    count?: number | undefined,
    orderBy?: OrderByCountEnum | undefined
    orderDir?: OrderDirEnum | undefined
}

export interface Paginated<T> {
    count: number,
    offset: number,
    totalcount: number,
    records: T[]
}
