/*-
 * Copyright (C) 2019 - 2021 ABACO
 *
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
 */

import { http, BASE, listPropsToDate } from './HTTP';
import { Contract, ContractLandUse, ContractMonitoringStatus, ContractOut } from './models/Contract';

export default class ContractsResource {
    public static async getContracts(partyId: string): Promise<Contract[]> {
        const response = await http.get(`${BASE}/contracts/farmers/${partyId}`)
        listPropsToDate(response.data, ["subscribtionDt", "expiryDate"]);
        return response.data;
    }

    public static async askForPayment(contractId: string): Promise<void> {
        const response = http.post(`${BASE}/contracts/${contractId}/ask-for-payment`)
        return (await response).data
    }

    public static async createContract(contractTypeId: string, partyId: string): Promise<ContractOut> {
        const response = http.post(`${BASE}/contracts`, {
            contractTypeId,
            partyId
        })
        return (await response).data
    }

    public static async getContract(contractId: string): Promise<Contract> {
        const response = http.get(`${BASE}/contracts/${contractId}`)
        return (await response).data;
    }

    public static async getMonitoringStatus(contractId: string): Promise<ContractMonitoringStatus> {
        const response = http.get(`${BASE}/contracts/${contractId}/monitoring-status`)
        const res = (await response).data;
        if (res.referenceDate !== null)
            res.referenceDate = new Date(res.referenceDate);
        return res;
    }

    public static async getContractLanduses(contractId: string): Promise<ContractLandUse[]> {
        const response = http.get(`${BASE}/contracts/${contractId}/land-uses`)
        return (await response).data;
    }

    public static async printPaymentContractURL(contractId: string): Promise<string> {
        const response = await http.get(`${BASE}/contracts/${contractId}/payment.pdf`,
            {
                responseType: "blob"
            }
        );
        return window.URL.createObjectURL(response.data);
    }

    public static async printContractURL(contractId: string): Promise<string> {
        const response = await http.get(`${BASE}/contracts/${contractId}/contract.pdf`,
            {
                responseType: "blob"
            }
        );
        return window.URL.createObjectURL(response.data);
    }

    public static async printContract(contractId: string) {
        const response = await http.get(`${BASE}/contracts/${contractId}/contract.pdf`,
            {
                responseType: "blob"
            }
        );
        const url = window.URL.createObjectURL(response.data);
        window.open(url);
        setTimeout(() => window.URL.revokeObjectURL(url), 100);
    }

    public static async updateContract(contractId: string) {
        await http.delete(`${BASE}/contracts/${contractId}`)
    }

    /**
     * Temporary methods
     * @param contractType
     * @param partyId
     */
    public static async getContractCreated(contractType: string, partyId: string): Promise<Contract>  {
        const response = await http.get(`${BASE}/contracts/farmers/${partyId}`)
        listPropsToDate(response.data, ["subscribtionDt","expiryDate"]);
        const filtered = response.data.filter((c: Contract)=>{
            return c.contractTypeId == contractType
        })
        const a = filtered.reduce((prev: Contract, curr: Contract) => {
            return curr.subscribtionDt.getTime() > prev.subscribtionDt.getTime() ? curr : prev
        })
        return a
    }
    /**
     * Temp methods
     */
    public static async getAllContracts(): Promise<Contract[]> {
        //TODO
        const response = await http.get(`${BASE}/contracts/farmers/1`)
        listPropsToDate(response.data, ["subscribtionDt", "expiryDate"]);
        return response.data;
    }
}
