/*-
 * Copyright (C) 2019 - 2021 ABACO
 *
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
 */

import { http, BASE, listPropsToDate } from './HTTP';
import { Contract } from './models/Contract';
import { Paginated, ParamPaginated } from './models/Datatable';

export default class PAContractsResource {

    public static async getContracts(): Promise<Contract[]> {
        const response = await http.get(`${BASE}/contracts`)
        listPropsToDate(response.data, ["subscribtionDt", "expiryDate"]);
        return response.data;
    }

    public static async getPaginatedContracts(queryParamPaginated: ParamPaginated | null): Promise<Paginated<Contract>> {
        const response = await http.get(`${BASE}/contracts`,
        {
            params: queryParamPaginated
        })

        const result: Paginated<Contract> = response.data;
        if (!result.records || !result.records.length) {
            result.records = [];
            result.count = 0;
            result.offset = 0;
            result.totalcount = 0;
        }
        listPropsToDate(result.records, ["subscribtionDt", "expiryDate"]);

        return result;
    }

}
