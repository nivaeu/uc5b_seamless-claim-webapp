/*-
 * Copyright (C) 2019 - 2021 ABACO
 *
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
 */

import { http, BASE } from './HTTP';
import { Party } from './models/Party';

export default class RegistryResource {

    public static async getParties(search: string | null): Promise<Party[]>{
        const response = http.get(`${BASE}/registry/parties`, {
            params:{
                search
            }
        })
        return (await response).data
    }

}
