/*-
 * Copyright (C) 2019 - 2021 ABACO
 *
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
 */

import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import store from "@/store";
import { UserSingleton } from "@/store/currentUser";
import { cancelTokenSources } from "@/api/HTTP";


Vue.use(VueRouter)


const routes: Array<RouteConfig> = [
    {
        path: "/",
        component: () => import("@/components/Home.vue"),
        children: [
            {
                path: "/",
                name: "SearchRegistry",
                component: () => import("@/components/registry/Search.vue"),
                meta: {
                    breadcumbs: [
                        { name: "farmers" },
                    ]
                },
                beforeEnter: (to, from, next) => {
                    if (store.state.user?.backendUser) {
                        next({ name: 'AdminMenu' })
                    } else {
                        next()
                    }
                }
            },
            {
                path: "/admin",
                name: "AdminContent",
                component: () => import("@/components/backoffice/AdminContent.vue"),
                meta: {
                    breadcumbs: [
                        { name: "adminMenu" },
                    ]
                },
                beforeEnter: (to, from, next) => {
                    if (!store.state.user?.backendUser) {
                        next({ name: 'SearchRegistry' })
                    } else {
                        next()
                    }
                },
                children:[
                    {
                        path: "/admin/",
                        name: "AdminMenu",
                        component: () => import("@/components/backoffice/AdminMenu.vue"),
                        meta: {
                            breadcumbs: [
                                { name: "adminMenu", link: "AdminMenu" },
                            ]
                        }
                    },
                    {
                        path: "/admin/search",
                        name: "AdminSearchRegistry",
                        component: () => import("@/components/backoffice/AdminSearch.vue"),
                        meta: {
                            breadcumbs: [
                                { name: "adminMenu", link: "AdminMenu" },
                            ]
                        }
                    },
                    {
                        path: "/admin/contracts",
                        name: "allContracts",
                        component: () => import("@/components/backoffice/AllContracts.vue"),
                        meta: {
                            breadcumbs: [
                                { name: "adminMenu", link: "AdminMenu" },
                                { name: ":curView" }
                            ]
                        },
                    },
                    {
                        path: "/admin/contracts/:partyId/:curView",
                        name: "AdminContractDetails",
                        props: true,
                        component: () => import("@/components/backoffice/AdminContractDetails.vue"),
                        meta: {
                            breadcumbs: [
                                { name: "adminMenu", link: "AdminMenu" },
                                { name: ":curView", link: ":curView" },
                                { name: ":curContract", noI18n: true}
                            ]
                        },
                    }
                ]
            },
            {
                path: "/:partyId",
                name: "Contracts",
                component: () => import("@/components/contracts/Contracts.vue"),
                meta: {
                    breadcumbs: [
                        { name: "farmers", link: "SearchRegistry" },
                        { name: "curView" }
                    ]
                },
                children: [
                    {
                        path: "/:partyId/:curView",
                        name: "myContracts",
                        component: () => import("@/components/contracts/MyContracts.vue"),
                        meta: {
                            breadcumbs: [
                                { name: "farmers", link: "SearchRegistry" },
                                { name: "myContracts" }
                            ]
                        },
                    },
                    {
                        path: "/:partyId/:curView",
                        name: "newContracts",
                        component: () => import("@/components/contracts/NewContracts.vue"),
                        meta: {
                            breadcumbs: [
                                { name: "farmers", link: "SearchRegistry" },
                                { name: "newContracts" }
                            ]
                        }
                    },
                ]
            },
            {
                path: "/:partyId/:curView/new",
                name: "NewContract",
                props: true,
                component: () => import("@/components/contracts/NewContract.vue"),
                meta: {
                    breadcumbs: [
                        { name: "farmers", link: "SearchRegistry" },
                        { name: ":curView", link: ":curView" },
                        { name: ":curContract", noI18n: true}
                    ]
                },
                children: [
                    {
                        path: "/:partyId/:curView/new/:curContract",
                        name: "NewContractDetails",
                        props: true,
                        component: () => import("@/components/contracts/NewContractDetails.vue"),
                        meta: {
                            breadcumbs: [
                                { name: "farmers", link: "SearchRegistry" },
                                { name: ":curView", link: ":curView" },
                                { name: ":curContract", noI18n: true}
                            ]
                        },
                    },
                    {
                        path: "/:partyId/:curView/new/:curContract",
                        name: "NewContractSubscribed",
                        props: true,
                        component: () => import("@/components/contracts/NewContractSubscribed.vue"),
                        meta: {
                            breadcumbs: [
                                { name: "farmers", link: "SearchRegistry" },
                                { name: ":curView", link: ":curView" },
                                { name: ":curContract", noI18n: true}
                            ]
                        },
                    }
                ]
            },
            {
                path: "/:partyId/:curView/:curContract",
                name: "ContractsDetails",
                props: true,
                component: () => import("@/components/contracts/ContractsDetails.vue"),
                meta: {
                    breadcumbs: [
                        { name: "farmers", link: "SearchRegistry" },
                        { name: ":curView", link: ":curView" },
                        { name: ":curContract", noI18n: true}
                    ]
                }
            },
            {
                path: "/:partyId/:curView/:curContract/landUses",
                name: "ContractLandUses",
                props: true,
                component: () => import("@/components/contracts/ContractLandUses.vue"),
                meta: {
                    breadcumbs: [
                        { name: "farmers", link: "SearchRegistry" },
                        { name: ":curView", link: ":curView" },
                        { name: ":curContract", link: "ContractsDetails", noI18n: true },
                        { name: "outcomeByParcel" }
                    ]
                }
            }
        ]
    },
    {
        path: "/login",
        name: "Login",
        meta: { guest: true },
        component: () => import("@/components/login/Login.vue"),
    }
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

router.beforeEach((to, from, next) => {
    if (to.meta?.guest || UserSingleton.instance()) {
        next();
    } else {
        next({ name: "Login" });
    }
});

router.afterEach(() => {
    for (const [cancelToken, cancel] of cancelTokenSources) {
        cancel(cancelToken);
    }
  });

export default router
