/*-
 * Copyright (C) 2019 - 2021 ABACO
 *
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
 */

import Vue from 'vue';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n);

const i18n = new VueI18n({
    locale: navigator.language,
    fallbackLocale: "en",
    messages: {
        en: require("@/../public/translations/en.json"),
        'it-IT': require("@/../public/translations/it.json"),
    }
});

export default i18n;
