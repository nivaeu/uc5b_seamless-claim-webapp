/*-
 * Copyright (C) 2019 - 2021 ABACO
 *
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
 */

import Vue from 'vue';
import Vuex from 'vuex';
import { Party } from './api/models/Party';
import { User } from './api/models/User';
import { ContractDetail } from "./api/models/Contract";
import { UserInfo, UserSingleton } from './store/currentUser';
import { UIError } from './utils/types';
import { MyContractsCached } from './api/cached/MyContractsCached';

Vue.use(Vuex);

const myContractsCached = new MyContractsCached();

const store = new Vuex.Store({
    state: {
        user: null as User | null,
        curParty: {} as Party,
        parties: [] as Party[],
        curContractDetail: {} as ContractDetail,
        curNewContractDetail: {} as ContractDetail,
        currAdminContractDetail: {} as ContractDetail,
        loading: false,
        error: {
            msg: "",
            showing: false
        } as UIError,
        token: (): string | null => {
            return UserSingleton.getToken();
        },
        myCurrentContracts: myContractsCached
    },

    getters: {
        username: (state): string | undefined => state.user?.username,

        token: (): string | null => {
            return UserSingleton.getToken();
        }
    },

    mutations: {
        closeUIError(state) {
            state.error = {
                showing: false,
                msg: ""
            } as UIError;
        },

        error(state, msg: string) {
            state.error = {
                showing: true,
                msg: msg
            } as UIError;
        },

        loading(state, loading: boolean) {
            state.loading = loading;
        },

        setCurrentUser(state, userInfo: UserInfo) {
            state.user = userInfo.user;
            UserSingleton.setUser(userInfo.user);
            UserSingleton.setToken(userInfo.token)
        },

        resetCurParty(state) {
            state.curParty = {} as Party;
        },

        logout() {
            UserSingleton.clear()
        },

        setCurParty(state, party: Party) {
            state.curParty = party;
        },

        setParties(state, parties: Party[]) {
            state.parties = parties;
        },

        setCurrentAdminContractDetail(state, curContractDetail: ContractDetail ) {
            state.currAdminContractDetail = curContractDetail;
        },

        setCurrentContractDetail(state, curContractDetail: ContractDetail ) {
            state.curContractDetail = curContractDetail;
        },

        setCurrentNewContractDetail(state, curContractDetail: ContractDetail ) {
            state.curNewContractDetail = curContractDetail;
        },

        resetNewCurrentContractDetail(state) {
            state.curNewContractDetail = {} as ContractDetail;
        },

        reset(state){
            state.user = null;
            state.curParty = {} as Party;
            state.parties = [] as Party[];
            state.curContractDetail = {} as ContractDetail;
            state.curNewContractDetail = {} as ContractDetail;
            state.myCurrentContracts.reset();
        }
    }
});

export default store;
