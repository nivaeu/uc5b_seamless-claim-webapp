import { User } from "@/api/models/User";
import { NivaLS } from "@/utils/localstorage";

const USER = "u";
const TOKEN = "_t";

export interface UserInfo {
    user: User;
    token: string;
}

export const UserSingleton = (() => {
    let instance: User | null;
    return {
        instance: (): User | null => {
            return instance;
        },
        setUser: (user: User) => {            
            instance = user;
            NivaLS.storeObject(USER, user);
        },
        getToken:() => {
            return NivaLS.getStoredObject(TOKEN, null);
        },
        setToken: (token: string) => {
            NivaLS.storeObject(TOKEN, token);
        },
        clear() {
            NivaLS.removeObject(USER);
            NivaLS.removeObject(TOKEN);
            instance = null;
        }
    }
})();