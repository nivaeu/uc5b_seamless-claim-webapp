/*-
 * Copyright (C) 2019 - 2021 ABACO
 *
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
 */

import Vue from "vue"
import Vuex from 'vuex'
import Toasted from "vue-toasted";

import App from "./App.vue"
import router from "./router"
import store from "./store"
import i18n from "./i18n";
import "@fortawesome/fontawesome-free/css/all.css";
import "@/assets/styles/index.css";

Vue.config.productionTip = false

Vue.use(Vuex)
Vue.use(Toasted, {
    position: "top-center",
    duration: "4000",
    keepOnHover: true
});


new Vue({
    router,
    store,
    i18n,
    render: h => h(App)
}).$mount("#app")
