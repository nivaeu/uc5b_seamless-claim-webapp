/*-
 * Copyright (C) 2019 - 2021 ABACO
 *
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
 */

const dateToString = (dtt: any): string => {
    try {
        let dt = dtt as Date;
        if (typeof dt == "string")
            dt = new Date(dt);

        return dt.toLocaleString().split(",")[0];
    } catch (error) {
        try {
            return dtt.toLocaleString();
        } catch (error) {
            return dtt;
        }
    }
}

export {
    dateToString
}
