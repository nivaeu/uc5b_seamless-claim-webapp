/*-
 * Copyright (C) 2019 - 2021 ABACO
 *
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
 */

export class NivaLS {
    /**
     * Retrieve an object from the local HTML5 storage.
     */
    public static getStoredObject(key: string, defaultValue: any): any {
        if (typeof (Storage) !== "undefined") {
            const json = localStorage.getItem(key);
            if (json != null && json != "") {
                try {
                    return JSON.parse(json);
                } catch (error) {
                    return defaultValue;
                }
            }
        }
        return defaultValue;
    }

    /**
     * Save an object in the local HTML5 storage.
     * @param key
     * @param value
     */
    public static storeObject(key: string, value: any): boolean {
        if (typeof (Storage) !== "undefined") {
            if (value != null) {
                try {
                    localStorage.setItem(key, JSON.stringify(value));
                } catch (error) {
                    return false;
                }
            } else
                localStorage.removeItem(key);
            return true;
        } else {
            return false;
        }
    }

    /**
 * Save an object in the local HTML5 storage.
 * @param key
 * @param value
 */
    public static removeObject(key: string): boolean {
        if (typeof (Storage) !== "undefined") {
            try {
                localStorage.removeItem(key);
                return true;
            } catch (error) {
                return false;
            }
        } else {
            return false;
        }
    }
}
